<?php

use yii\db\Migration;

/**
 * Class m231205_124949_add_products
 */
class m231205_124949_add_products extends Migration
{
    private array $products = [
        'апельсины', 'бананы', 'картошка', 'кефир', 'кофе', 'лук',
        'масло', 'молоко', 'мука', 'огурцы', 'перцы', 'помидоры',
        'рис', 'сахар', 'сыр', 'творог', 'чай', 'яблоки',
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = [];

        foreach ($this->products as $product) {
            $rows[] = [
                $product,
                date('Y-m-d H:i:s'),
                date('Y-m-d H:i:s')
            ];
        }

        $this->batchInsert('product', ['name', 'created_at' , 'updated_at'], $rows);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('product', ['in', 'name', $this->products]);
        \Yii::$app->db->createCommand('alter table product AUTO_INCREMENT = 1')->execute();
    }

}
