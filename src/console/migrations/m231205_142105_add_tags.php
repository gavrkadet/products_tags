<?php

use yii\db\Migration;

/**
 * Class m231205_142105_add_tags
 */
class m231205_142105_add_tags extends Migration
{
    private array $tags = [
        //-----------------Акции------------------
        [
            'Скидка',       //  название тега
            1,              //  приоритет
            1               //  id группы тегов
        ],
        [
            'Кешбек 10%',   //  название тега
            2,              //  приоритет
            1               //  id группы тегов
        ],
        //-----------------Отделы----------------
        [
            'Молоко',
            3,
            2
        ],
        [
            'Бакалея',
            4,
            2
        ],
        [
            'Овощи фрукты',
            5,
            2
        ],
        //--------------------Инфо------------------
        [
            'Эко',
            6,
            3
        ],
        [
            'Для детей',
            7,
            3
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = [];
        foreach ($this->tags as $tag) {
            $tag[] = date('Y-m-d H:i:s');
            $tag[] = date('Y-m-d H:i:s');
            $rows[] = $tag;
        }

        $this->batchInsert('tag', ['name', 'priority', 'group_id', 'created_at' , 'updated_at'], $rows);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('tag_group');
        \Yii::$app->db->createCommand('alter table tag AUTO_INCREMENT = 1')->execute();
    }

}
