<?php

use yii\db\Migration;

/**
 * Class m231204_164016_add_product_tag_table
 */
class m231204_164016_add_product_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('product_tag', [
            'product_id' => $this->integer(255)->notNull(),
            'tag_id' => $this->integer(255)->notNull(),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создания'),
            'updated_at' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ], $tableOptions);

        $this->addPrimaryKey('product_tag_id',
            'product_tag',
            ['product_id', 'tag_id']);

        $this->addForeignKey(
            'product_id-fk',
            'product_tag',
            'product_id',
            'product',
            'id',
            'CASCADE',
            'CASCADE');

        $this->addForeignKey(
            'tag_id-fk',
            'product_tag',
            'tag_id',
            'tag',
            'id',
            'CASCADE',
            'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tag_id-fk', 'product_tag');
        $this->dropForeignKey('product_id-fk', 'product_tag');
        $this->dropPrimaryKey('product_tag_id', 'product_tag');
        $this->dropTable('product_tag');
    }

}
