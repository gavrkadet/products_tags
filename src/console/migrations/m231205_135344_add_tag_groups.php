<?php

use yii\db\Migration;

/**
 * Class m231205_135344_add_tag_groups
 */
class m231205_135344_add_tag_groups extends Migration
{
    private array $tagGroups = [
        [
            'Акции',
            'orange',
        ],
        [
            'Отделы',
            'blue'
        ],
        [
            'Инфо',
            'green'
        ],
    ];
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = [];

        foreach ($this->tagGroups as $tagGroup) {
            $tagGroup[] = date('Y-m-d H:i:s');
            $tagGroup[] = date('Y-m-d H:i:s');
            $rows[] = $tagGroup;
        }

        $this->batchInsert('tag_group', ['name', 'color', 'created_at' , 'updated_at'], $rows);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('tag_group');
        \Yii::$app->db->createCommand('alter table tag_group AUTO_INCREMENT = 1')->execute();
    }

}
