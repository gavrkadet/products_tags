<?php

use yii\db\Migration;

/**
 * Class m231205_162016_add_product_tags
 */
class m231205_162016_add_product_tags extends Migration
{
    private array $productTags = [
        [1, 1], [1, 5],
        [2, 5],
        [3, 2],
        [3, 5], [3, 6],
        [4, 3],
        [5, 1], [5, 2], [5, 4], [5, 6],
    ];
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = [];
        foreach ($this->productTags as $productTag) {
            $productTag[] = date('Y-m-d H:i:s');
            $productTag[] = date('Y-m-d H:i:s');
            $rows[] = $productTag;
        }

        $this->batchInsert('product_tag', ['product_id', 'tag_id', 'created_at' , 'updated_at'], $rows);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('product_tag');
        \Yii::$app->db->createCommand('alter table product_tag AUTO_INCREMENT = 1')->execute();

    }

}
