<?php

use yii\db\Migration;

/**
 * Class m231204_153949_add_tag_table
 */
class m231204_153949_add_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('tag', [
            'id' => $this->primaryKey(),
            'name' => $this->string('20')->notNull()->comment('Название тега'),
            'priority' => $this->integer('10')->notNull()->comment('Приоритет'),
            'group_id' => $this->integer(11)->notNull()->comment('Тип тега'),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создания'),
            'updated_at' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ], $tableOptions);

        $this->addForeignKey(
            'group_id-fk',
            'tag',
            'group_id',
            'tag_group',
            'id',
            'CASCADE',
            'CASCADE');

        $this->createIndex('group_id-idx', 'tag', 'group_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('group_id-fk', 'tag');
        $this->dropIndex('group_id-idx', 'tag');
        $this->dropTable('tag');
    }

}
