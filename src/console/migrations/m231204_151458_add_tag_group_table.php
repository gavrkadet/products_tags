<?php

use yii\db\Migration;

/**
 * Class m231204_151458_add_tag_group_table
 */
class m231204_151458_add_tag_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('tag_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string('20')->notNull()->comment('Название типа тега'),
            'color' => $this->string(20)->notNull()->comment('Цвет'),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создание'),
            'updated_at' => $this->dateTime()->notNull()->comment('Дата изменения'),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag_group');
    }

}
