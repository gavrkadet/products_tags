<?php

declare(strict_types=1);

namespace common\models;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $name Название тега
 * @property int $priority Приоритет
 * @property int $group_id Тип тега
 * @property string $created_at Дата создания
 * @property string $updated_at Дата изменения
 *
 * @property TagGroup $group
 * @property ProductTag[] $productTags
 * @property Product[] $products
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'priority', 'group_id', 'created_at', 'updated_at'], 'required'],
            [['priority', 'group_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 20],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TagGroup::class, 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'priority' => 'Приоритет',
            'group_id' => 'Группа тегов',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * Gets query for [[Group]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TagGroup::class, ['id' => 'group_id']);
    }

    /**
     * Gets query for [[ProductTags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductTags()
    {
        return $this->hasMany(ProductTag::class, ['tag_id' => 'id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])->viaTable('product_tag', ['tag_id' => 'id']);
    }
}
