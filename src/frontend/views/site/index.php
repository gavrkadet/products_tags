<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="p-5 mb-4 bg-transparent rounded-3">
        <div class="container-fluid py-5 text-center">
            <h1 class="display-4">Доброго времени суток</h1>
            <p class="fs-5 fw-light">Для того, чтобы увидеть список товаров с тегами, нажмите на кнопку</p>
            <p><a class="btn btn-lg btn-success" href="http://pt-front/product/index">Смотреть товары</a></p>
        </div>
    </div>
</div>
