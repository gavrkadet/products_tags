<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\TagGroup $model */

$this->title = 'Update Tag Group: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tag Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tag-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
