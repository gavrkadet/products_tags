<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\TagGroup $model */

$this->title = 'Create Tag Group';
$this->params['breadcrumbs'][] = ['label' => 'Tag Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
