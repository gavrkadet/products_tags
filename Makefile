up:
	docker compose up --force-recreate --remove-orphans --build # --detach
up-detach:
	docker compose up --force-recreate --remove-orphans --build --detach
down:
	docker compose down
restart:
	make down && make up-detach
php-sh:
	docker compose exec php-fpm sh
nginx-sh:
	docker compose exec nginx sh
db-sh:
	docker compose exec db sh